import socket

HOST = 'ru.wikipedia.org'
PORT = 80
BUFSIZ = 16384
ENCODING = 'utf-8'
ADDR = (HOST, PORT)

tcpCliSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpCliSock.connect(ADDR)

message = '''GET /wiki/HTTP HTTP/1.1\r
Host: ru.wikipedia.org\r
\r\n'''

tcpCliSock.send(message.encode(ENCODING))

response = []
while True:
    data = tcpCliSock.recv(BUFSIZ)
    if not data:
        break
    response.append(data)

print(b''.join(response).decode(ENCODING), end='')

tcpCliSock.close()
