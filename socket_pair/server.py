import socket

HOST = 'localhost'
PORT = 21572
BUFSIZ = 1024
ENCODING = 'utf-8'
ADDR = (HOST, PORT)

serversock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serversock.bind(ADDR)
serversock.listen(2)

while 1:
    print('waiting for connection...')
    clientsock, addr = serversock.accept()
    print('...connected from:', addr)

    while 1:
        data = clientsock.recv(BUFSIZ).decode(ENCODING)
        if not data:
            break
        print('received:', data)
        clientsock.send(('echoed: ' + data).encode(ENCODING))

    clientsock.close()

serversock.close()
