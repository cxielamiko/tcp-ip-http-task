import socket

HOST = 'localhost'
PORT = 21572
BUFSIZ = 1024
ENCODING = 'utf-8'
ADDR = (HOST, PORT)

tcpCliSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpCliSock.connect(ADDR)

while True:
    data = input('> ')
    if not data:
        break
    tcpCliSock.send(data.encode(ENCODING))
    data = tcpCliSock.recv(BUFSIZ).decode(ENCODING)
    if not data:
        break
    print(data)

tcpCliSock.close()
